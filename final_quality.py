'''
	This module is used for image quality assessment of given image it calculate
	image blurriness index, luminance coefficients, brisque score
'''

import os
from cv2.quality import QualityBRISQUE_compute
from cv2 import split, Laplacian, CV_64F
from google_images_download import google_images_download

def calc_brightness(img, debug=0):
    '''
        this function calculate luminance coefficient.
    '''
	#((Red value X 299) + (Green value X 587) + (Blue value X 114)
    color = split(img)
    color[2] = color[2].flatten() * 0.299
    color[1] = color[1].flatten() * 0.587
    color[0] = color[0].flatten() * 0.114
    lum = color[0] + color[1] + color[2]
    lum_coeff = sum(lum)
    brightness = round(lum_coeff / (((2**8)-1)*img.shape[0] * img.shape[1]) * 100)
    if debug == 1:
        return brightness
    # This range is calculated from averaging 115 images from google
    if brightness >= 33 and brightness <= 82:
        return "normal"
    elif brightness > 82:
        return"over"
    return "under"

def calc_blurriness(img, debug=0):
    '''
        This function calculate blurriness coefficient.
    '''
    score = round(Laplacian(img, CV_64F).var())
    if debug == 1:
        return score
    # reference from pyimagesearch 
    # https://www.pyimagesearch.com/2015/09/07/blur-detection-with-opencv/
    if score < 100:
        return -1
    return 1

def cal_brisque(img, debug=0):
    '''
        This function calculate brisque score
    '''
    brisque_index = round(QualityBRISQUE_compute([img], "brisque_model_live.yml",
                                                 "brisque_range_live.yml")[0])
    if debug == 1:
        return brisque_index
    # This range is calculated from averaging 115 images from google
    if brisque_index > 42 and brisque_index < 0:
        return -1
    return 1

def process_folder(path):
    '''
        This function used for batch processing
    '''
    list_of_file = os.listdir(path)
    for item in list_of_file:
        quality_image = quality(path + item)
        print(quality_image)

def quality(img, debug=0):
    '''
        This function return true or false if image quality is good or bad
	debug option for accessing number values calculated for the image
    '''
    bright = calc_brightness(img, debug)
    blur = calc_blurriness(img, debug)
    brisq = cal_brisque(img, debug)
    if debug == 1:
        return bright, blur, brisq
    if bright == "normal" and blur == 1 and brisq == 1:
        return True
    return False

def scraping():
    '''
        Function to scrape images from google
    '''
    response = google_images_download.googleimagesdownload()
    arguments = {"keywords":"good female face", "limit":500, "output_directory":"test/",
                 "chromedriver":"/usr/lib/chromium-browser/chromedriver"}
    response.download(arguments)
