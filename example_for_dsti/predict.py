import keras
from keras_bert import Tokenizer
from keras_bert import AdamWarmup, calc_train_steps
from keras_bert import load_trained_model_from_checkpoint
import os
import numpy as np
import tensorflow as tf
import re


def preprocess(sen):
    sentence = re.sub('[^a-zA-Z]', ' ', sen)
    sentence = re.sub(r"\s+[a-zA-Z]\s+", ' ', sentence)
    sentence = re.sub(r'\s+', ' ', sentence)
    return sentence


def init(bundle_path):
    pretrained_path = os.path.join(bundle_path,'uncased_L-12_H-768_A-12')
    config_path = os.path.join(pretrained_path, 'bert_config.json')
    checkpoint_path = os.path.join(pretrained_path, 'bert_model.ckpt')
    vocab_path = os.path.join(pretrained_path, 'vocab.txt')
    NUMBER_LABEL=6
    f = open(vocab_path)
    k = f.read()
    z=k.split("\n")
    token = dict()
    for m in range(len(z)):
        token[z[m]]=m
    SEQ_LEN = 128
    global model
    model = load_trained_model_from_checkpoint(
        config_path,
        checkpoint_path,
        training=True,
        trainable=True,
        seq_len=SEQ_LEN,
    )
    inputs = model.inputs[:2]
    dense = model.get_layer('NSP-Dense').output

    outputs = keras.layers.Dense(units=6, activation='sigmoid')(dense)
    model = keras.Model(inputs, outputs)
    model.load_weights(os.path.join(bundle_path,'model.h5'))
    global tokenizer
    tokenizer = Tokenizer(token)
    

def predict(inputs):
    if "input" not in inputs:
        raise ValueError("Not Found")
    text = [ tokenizer.encode(preprocess(inputs['input']),max_len=128)[0],np.array([0]*128) ]
    return {'out':model.predict(np.array(text).reshape(2,1,128).tolist())[0].tolist()}
