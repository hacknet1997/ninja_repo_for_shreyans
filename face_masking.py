import cv2
import dlib
import imutils
import matplotlib.pyplot as plt
import numpy as np
from imutils import face_utils
import os
import imutils
from final_quality import quality


def predict(img):
    '''
        Thsi function predict the faces in image
        return shape and rectangle information
    '''
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    rects = detector(gray, 1)
    for (i, rect) in enumerate(rects):
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
    return shape,rects

def fine_tune(rects,shape,mask,img):
    '''
        This function return the masked face image
    '''
    for (name, (i, j)) in face_utils.FACIAL_LANDMARKS_IDXS.items():  
        hull = cv2.convexHull(shape[i:j])
        if name in ["left_eyebrow","right_eyebrow","mouth","left_eye","right_eye"]:
            cv2.drawContours(mask, [hull], -1, (0,0,0), -1)
    im = np.bitwise_and(img,mask)
    return im

def smoothing(img):
    '''
        This function perform smoothing on the image
    '''
    return cv2.medianBlur(img,5)

def back_extract(img,shape):
    '''
        This function takes numpy image and extract
        the foredround from the given image.
    '''
    mask = np.zeros(img.shape[:2],np.uint8)
    bgdModel = np.zeros((1,65),np.float64)
    fgdModel = np.zeros((1,65),np.float64)
    rect = (shape[0][0],shape[0][1]-int(0.2*img.shape[0]),shape[15][0]-shape[0][0],
            shape[9][1]-(max((shape[15][1],shape[0][1]))-int(0.33*img.shape[0])))
    print(rect)
    cv2.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)
    mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
    img = img*mask2[:,:,np.newaxis]
    return img

def process_image(img, blur=False):
    '''
        This function takes numpy array and return the masked face from image
    '''
    check,error = quality(img,-1)
    if check:
        shape, rect = predict(img)
        for (i,n) in enumerate(rect):
            x,y,w,h=face_utils.rect_to_bb(n)
            imgk = back_extract(img,shape)
            k=fin(rect,shape,np.full_like(imgk,255),imgk)[y:y+h,x:x+w]
            if blur:
                k = smoothing(k)
            return k
    return error
