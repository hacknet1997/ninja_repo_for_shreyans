import cv2
import dlib
import imutils
import matplotlib.pyplot as plt
import numpy as np
from imutils import face_utils


img = cv2.imread("face.jpg")


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
 
# detect faces in the grayscale image
rects = detector(gray, 1)


pts=[]
for (i, rect) in enumerate(rects):

    shape = predictor(gray, rect)
    shape = face_utils.shape_to_np(shape) 

    for (name, (i, j)) in face_utils.FACIAL_LANDMARKS_IDXS.items():
        if name in ['left_eye','right_eye']:    
            hull = cv2.convexHull(shape[i:j])
            pts.append(cv2.boundingRect(hull))

roi = img[pts[0][1]:pts[0][1] + pts[0][3], pts[0][0]:pts[0][0] + pts[0][2]]



gray_roi=cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY)
mask = np.full_like(roi,255)

rows, cols,_ = mask.shape
# create a black filled circle
mask=cv2.circle(mask, (int(roi.shape[1]/2),int(roi.shape[0]/2)),10 , (0,0,0),-1)
# Bitwise AND operation to black out regions outside the mask
result = np.bitwise_and(roi,mask)

w=roi.shape
gray_roi = cv2.bilateralFilter(gray_roi, 11, 17, 17)
cv2.circle(roi, (int(w[1]/2),int(w[0]/2)),10 , (255, 0, 0))


#Red = 164, Green = 162, Blue = 173
res_gray=cv2.cvtColor(result,cv2.COLOR_BGR2GRAY)


k,b=cv2.threshold(res_gray, 10, 255, cv2.THRESH_OTSU)
kkk= np.bitwise_and(res_gray,b)
kkk = np.bitwise_and(kkk,res_gray)
cv2.imwrite('ahh.jpg',kkk)

im = cv2.imread('ahh.jpg')

plt.hist(im.ravel(),256,[0,256]) 
plt.show() 

