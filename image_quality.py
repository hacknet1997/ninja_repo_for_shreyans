'''
	This module is used for image quality assessment of given image it calculate
	image blurriness index, luminance coefficients, brisque score
'''

import os
from cv2.quality import QualityBRISQUE_compute
from cv2 import split, Laplacian, CV_64F

def calc_brightness(img):
    '''
        this function calculate luminance coefficient.
    '''
	#((Red value X 299) + (Green value X 587) + (Blue value X 114)
    color = split(img)
    color[2] = color[2].flatten() * 0.299
    color[1] = color[1].flatten() * 0.587
    color[0] = color[0].flatten() * 0.114
    lum = color[0] + color[1] + color[2]
    lum_coeff = sum(lum)
    brightness = lum_coeff / (((2**8)-1)*img.shape[0] * img.shape[1]) * 100
    if brightness > 34 and brightness < 82:
        return "normal"
    elif brightness > 82:
        return"over"
    return "under"

def calc_blurriness(img):
    '''
        This function calculate blurriness coefficient.
    '''
    score = Laplacian(img, CV_64F).var()
    if score < 100:
        return -1
    return 1

def cal_brisque(img):
    '''
        This function calculate brisque score
    '''
    return QualityBRISQUE_compute([img],
                                  "model_files/brisque_model_live.yml",
                                  "model_files/brisque_range_live.yml")

def process_folder(path):
    '''
        This function used for batch processing
    '''
    list_of_file = os.listdir(path)
    for item in list_of_file:
        quality_image = quality(path + item)
        print(quality_image)

def quality(img):
    '''
        This function return true or false if image quality is good or bad
    '''
    bright = calc_brightness(img)
    blur = calc_blurriness(img=img)
    if bright == "normal" and blur == 1:
        return True
    return False
