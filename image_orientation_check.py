'''
	Module to check face orientation in image
'''
import cv2
import dlib
from imutils import face_utils

def check(img):
    '''
        This function takes numpy image array 
        for checking face orientation in it.
    '''
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor("model_files/shape_predictor_5_face_landmarks.dat")
    try:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        rects = detector(gray, 1)
        print(rects)
        for rect in rects:
            shape = predictor(gray, rect)
            shape = face_utils.shape_to_np(shape)
        left_eye = shape[0][0]
        right_eye = shape[2][0]
        width = img.shape[1]
        dist = width - right_eye
        k = max((left_eye,dist))
        error = int(0.1 * k)
        print(error,abs(left_eye-dist))
        if left_eye==dist or abs(left_eye-dist)<=error:
            return "center"
        elif dist>left_eye:
            return "left"
        else:
            return("right")
    except Exception as e:
        print("e")
