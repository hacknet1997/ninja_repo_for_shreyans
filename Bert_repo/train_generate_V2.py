import json
import pandas as pd
import os, sys
try:
    if sys.argv[1] == None:
        FOLDER_NAME = "dataset/"
    else:
        FOLDER_NAME = sys.argv[1]
except:
    FOLDER_NAME = "dataset/"
ls_file = os.listdir(FOLDER_NAME)
dic = {"comment_text":[]}
label = [ky[:ky.index("_training_data.json")] for ky in ls_file]
k = {ky:v for ky,v in zip(label,ls_file)}
for l in label:
    dic[l] = []
for ky,f in k.items():
    kf = json.load(open(FOLDER_NAME+f))
    temp = 0
    for key in kf["rasa_nlu_data"]["common_examples"]:
        temp += 1
        dic['comment_text'].append(key['text'])
    for la in label:
        for l in range(temp):
            if la != ky:
                dic[la].append(0)
            else:
                dic[la].append(1)
df = pd.DataFrame(dic)
df.to_csv("train.csv",index_label="id")

