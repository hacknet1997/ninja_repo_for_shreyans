import json
k = json.load("training_dataset.json")

ls = list()

for key in k["rasa_nlu_data"]["common_examples"]:
	ls.append(key["text"])

ls = ls[:len(ls)-5]
id_ls = list(range(len(ls)))

book_ls = [1]*len(ls)
hr_ls = [0]*len(ls)

di = dict()

di["id"] = id_ls
di["text"] = ls
di["book_room"] = book_ls

di["hr"] = hr_ls

import pandas as pd
df = pd.DataFrame(di)

df.to_csv("train.csv")
